-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 27, 2020 at 10:15 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bumdes`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Admin', '1', 1585228809),
('Operator', '3', 1585229201);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1585228049, 1585228049),
('/barang/*', 2, NULL, NULL, NULL, 1585228558, 1585228558),
('/barang/create', 2, NULL, NULL, NULL, 1585228559, 1585228559),
('/barang/delete', 2, NULL, NULL, NULL, 1585228561, 1585228561),
('/barang/index', 2, NULL, NULL, NULL, 1585228628, 1585228628),
('/barang/view', 2, NULL, NULL, NULL, 1585228556, 1585228556),
('Admin', 1, NULL, NULL, NULL, 1585228046, 1585228046),
('Operator', 1, NULL, NULL, NULL, 1585227991, 1585227991);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Admin', '/barang/*'),
('Operator', '/barang/create'),
('Operator', '/barang/index'),
('Operator', '/barang/view');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `masalah_bumdes`
--

CREATE TABLE `masalah_bumdes` (
  `id_bumdes` int(11) NOT NULL,
  `id_masalah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1585226700),
('m130524_201442_init', 1585226704),
('m170228_064223_rbac_create', 1585226705),
('m170228_064635_mimin_init', 1585226705),
('m190720_083956_createtablebarang', 1585226705);

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE `route` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`name`, `alias`, `type`, `status`) VALUES
('/*', '*', '', 1),
('/barang/*', '*', 'barang', 1),
('/barang/create', 'create', 'barang', 1),
('/barang/delete', 'delete', 'barang', 1),
('/barang/index', 'index', 'barang', 1),
('/barang/update', 'update', 'barang', 1),
('/barang/view', 'view', 'barang', 1),
('/datecontrol/*', '*', 'datecontrol', 1),
('/datecontrol/parse/*', '*', 'datecontrol/parse', 1),
('/datecontrol/parse/convert', 'convert', 'datecontrol/parse', 1),
('/debug/*', '*', 'debug', 1),
('/debug/default/*', '*', 'debug/default', 1),
('/debug/default/db-explain', 'db-explain', 'debug/default', 1),
('/debug/default/download-mail', 'download-mail', 'debug/default', 1),
('/debug/default/index', 'index', 'debug/default', 1),
('/debug/default/toolbar', 'toolbar', 'debug/default', 1),
('/debug/default/view', 'view', 'debug/default', 1),
('/debug/user/*', '*', 'debug/user', 1),
('/debug/user/reset-identity', 'reset-identity', 'debug/user', 1),
('/debug/user/set-identity', 'set-identity', 'debug/user', 1),
('/gii/*', '*', 'gii', 1),
('/gii/default/*', '*', 'gii/default', 1),
('/gii/default/action', 'action', 'gii/default', 1),
('/gii/default/diff', 'diff', 'gii/default', 1),
('/gii/default/index', 'index', 'gii/default', 1),
('/gii/default/preview', 'preview', 'gii/default', 1),
('/gii/default/view', 'view', 'gii/default', 1),
('/gridview/*', '*', 'gridview', 1),
('/gridview/export/*', '*', 'gridview/export', 1),
('/gridview/export/download', 'download', 'gridview/export', 1),
('/mimin/*', '*', 'mimin', 1),
('/mimin/role/*', '*', 'mimin/role', 1),
('/mimin/role/create', 'create', 'mimin/role', 1),
('/mimin/role/delete', 'delete', 'mimin/role', 1),
('/mimin/role/index', 'index', 'mimin/role', 1),
('/mimin/role/permission', 'permission', 'mimin/role', 1),
('/mimin/role/update', 'update', 'mimin/role', 1),
('/mimin/role/view', 'view', 'mimin/role', 1),
('/mimin/route/*', '*', 'mimin/route', 1),
('/mimin/route/create', 'create', 'mimin/route', 1),
('/mimin/route/delete', 'delete', 'mimin/route', 1),
('/mimin/route/generate', 'generate', 'mimin/route', 1),
('/mimin/route/index', 'index', 'mimin/route', 1),
('/mimin/route/update', 'update', 'mimin/route', 1),
('/mimin/route/view', 'view', 'mimin/route', 1),
('/mimin/user/*', '*', 'mimin/user', 1),
('/mimin/user/create', 'create', 'mimin/user', 1),
('/mimin/user/delete', 'delete', 'mimin/user', 1),
('/mimin/user/index', 'index', 'mimin/user', 1),
('/mimin/user/update', 'update', 'mimin/user', 1),
('/mimin/user/view', 'view', 'mimin/user', 1),
('/role/*', '*', 'role', 1),
('/role/create', 'create', 'role', 1),
('/role/delete', 'delete', 'role', 1),
('/role/index', 'index', 'role', 1),
('/role/permission', 'permission', 'role', 1),
('/role/update', 'update', 'role', 1),
('/role/view', 'view', 'role', 1),
('/route/*', '*', 'route', 1),
('/route/create', 'create', 'route', 1),
('/route/delete', 'delete', 'route', 1),
('/route/generate', 'generate', 'route', 1),
('/route/index', 'index', 'route', 1),
('/route/update', 'update', 'route', 1),
('/route/view', 'view', 'route', 1),
('/site/*', '*', 'site', 1),
('/site/about', 'about', 'site', 1),
('/site/captcha', 'captcha', 'site', 1),
('/site/contact', 'contact', 'site', 1),
('/site/error', 'error', 'site', 1),
('/site/index', 'index', 'site', 1),
('/site/login', 'login', 'site', 1),
('/site/logout', 'logout', 'site', 1),
('/site/request-password-reset', 'request-password-reset', 'site', 1),
('/site/reset-password', 'reset-password', 'site', 1),
('/site/signup', 'signup', 'site', 1),
('/user/*', '*', 'user', 1),
('/user/create', 'create', 'user', 1),
('/user/delete', 'delete', 'user', 1),
('/user/index', 'index', 'user', 1),
('/user/update', 'update', 'user', 1),
('/user/view', 'view', 'user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_bumdes`
--

CREATE TABLE `tb_bumdes` (
  `id_bumdes` int(11) NOT NULL,
  `kode_bumdes` varchar(20) DEFAULT NULL,
  `nama_bumdes` varchar(200) NOT NULL,
  `nama_desa` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `no_perdes` varchar(200) NOT NULL,
  `th_perdes` int(11) NOT NULL,
  `adart` tinyint(1) NOT NULL DEFAULT '0',
  `rencana_men` tinyint(1) NOT NULL DEFAULT '0',
  `rencana_th` tinyint(1) NOT NULL DEFAULT '0',
  `sop` tinyint(1) NOT NULL DEFAULT '0',
  `catatan_keu` tinyint(1) NOT NULL DEFAULT '0',
  `jml_omset_th` double(15,2) NOT NULL DEFAULT '0.00',
  `kontribusi_pen_desa` double(15,2) NOT NULL DEFAULT '0.00',
  `jml_modal` double(15,2) NOT NULL DEFAULT '0.00',
  `jml_modal_lain` double(15,2) NOT NULL DEFAULT '0.00',
  `jml_kas` double(15,2) NOT NULL DEFAULT '0.00',
  `jml_aset_lcr` double(15,2) NOT NULL DEFAULT '0.00',
  `jml_aset_ttp` double(15,2) NOT NULL DEFAULT '0.00',
  `jml_utang` double(15,2) NOT NULL DEFAULT '0.00',
  `jml_utang_bank` double(15,2) NOT NULL DEFAULT '0.00',
  `jml_unit_ush` double(15,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `kode_kabupaten` varchar(100) DEFAULT NULL,
  `kode_kecamatan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kabupaten`
--

CREATE TABLE `tb_kabupaten` (
  `kd_kab` varchar(100) NOT NULL,
  `nama_kab` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kabupaten`
--

INSERT INTO `tb_kabupaten` (`kd_kab`, `nama_kab`) VALUES
('72.01', 'Kabupaten Banggai'),
('72.02', 'Kabupaten Poso'),
('72.03', 'Kabupaten Donggala'),
('72.04', 'Kabupaten Tolitoli'),
('72.05', 'Kabupaten Buol'),
('72.06', 'Kabupaten Morowali'),
('72.07', 'Kabupaten Banggai Kepulauan'),
('72.08', 'Kabupaten Parigi Moutong'),
('72.09', 'Kabupaten Tojo Una-Una'),
('72.10', 'Kabupaten Sigi'),
('72.11', 'Kabupaten Banggai Laut'),
('72.12', 'Kabupaten Morowali Utara'),
('72.71', 'Kota Palu');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(11) NOT NULL,
  `kode_kategori` varchar(20) NOT NULL,
  `nama_kategori` varchar(250) NOT NULL,
  `ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `kode_kategori`, `nama_kategori`, `ket`) VALUES
(1, 'U01', 'Bisnis Penyewaan', 'Penyewaan (Sewa alat transportasi, sewa gedung, dll)'),
(2, 'U02', 'Usaha Perantara', 'Perantara (Pembayaran Listrik, Pembayaran air, pasar desa, dll)'),
(3, 'U03', 'Bisnis Keuangan', 'Keuangan (Simpan Pinjam)'),
(4, 'U04', 'Bisnis Produksi dan Perdagangan', 'Perdagangan (Hasil pertanian, Sarana Produksi Pertanian, Toko, dll)'),
(5, 'U05', 'Bisnis Sosial', 'Sosial (air minum desa, usaha listrik desa, dll)'),
(6, 'U06', 'Kantor Pusat', ''),
(7, 'U07', 'Usaha Bersama', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kecamatan`
--

CREATE TABLE `tb_kecamatan` (
  `kd_kec` varchar(100) NOT NULL,
  `nama_kec` varchar(252) NOT NULL,
  `kd_kab` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kecamatan`
--

INSERT INTO `tb_kecamatan` (`kd_kec`, `nama_kec`, `kd_kab`) VALUES
('72.10.01', 'Sigi Biromaru', '72.10'),
('72.10.02', 'Palolo', '72.10'),
('72.10.03', 'Nokilalaki', '72.10'),
('72.10.04', 'Lindu', '72.10'),
('72.10.05', 'Kulawi', '72.10'),
('72.10.06', 'Kulawi Selatan', '72.10'),
('72.10.07', 'Pipikoro', '72.10'),
('72.10.08', 'Gumbasa', '72.10'),
('72.10.09', 'Dolo Selatan', '72.10'),
('72.10.10', 'Tanambulava', '72.10'),
('72.10.11', 'Dolo Barat', '72.10'),
('72.10.12', 'Dolo', '72.10'),
('72.10.13', 'Kinovaro', '72.10'),
('72.10.14', 'Marawola', '72.10'),
('72.10.15', 'Marawola Barat', '72.10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_masalah`
--

CREATE TABLE `tb_masalah` (
  `id_masalah` int(11) NOT NULL,
  `kode_masalah` varchar(20) NOT NULL,
  `nama_masalah` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_masalah`
--

INSERT INTO `tb_masalah` (`id_masalah`, `kode_masalah`, `nama_masalah`) VALUES
(1, 'M01', 'Permodalan'),
(2, 'M02', 'Kemampuan menggali potensi desa'),
(3, 'M03', 'Kemampuan menyusun laporan keuangan'),
(4, 'M04', 'Akses pasar/pemasaran'),
(5, 'M05', 'Akses terhadap bahan baku/supplier');

-- --------------------------------------------------------

--
-- Table structure for table `tb_prov`
--

CREATE TABLE `tb_prov` (
  `kd_prov` varchar(200) NOT NULL,
  `nama_prov` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_prov`
--

INSERT INTO `tb_prov` (`kd_prov`, `nama_prov`) VALUES
('72', 'Sulawesi Tengah');

-- --------------------------------------------------------

--
-- Table structure for table `tb_unit`
--

CREATE TABLE `tb_unit` (
  `id_unit` int(11) NOT NULL,
  `kode_unit` varchar(20) NOT NULL,
  `nama_unit` varchar(250) NOT NULL,
  `desc_unit` text NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `ket_unit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit_bumdes`
--

CREATE TABLE `unit_bumdes` (
  `id_bumdes` int(11) NOT NULL,
  `id_unit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Bj2xEpff-WmRLtY4TyHPHxRp6eAxsNZ0', '$2y$04$gYDTC7e9GzR.0XtqYF./guCEnBB2Mx..VxkKiB7U1l3F6BTF/hGF6', NULL, 'piant.grunger@gmail.com', 10, 1485769884, 1488270381),
(3, 'operator', 'Bj2xEpff-WmRLtY4TyHPHxRp6eAxsNZ0', '$2y$04$gYDTC7e9GzR.0XtqYF./guCEnBB2Mx..VxkKiB7U1l3F6BTF/hGF6', NULL, 'operator@gmail.com', 10, 1485769884, 1488270381);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `masalah_bumdes`
--
ALTER TABLE `masalah_bumdes`
  ADD PRIMARY KEY (`id_masalah`,`id_bumdes`),
  ADD KEY `id_masalah` (`id_masalah`),
  ADD KEY `id_bumdes` (`id_bumdes`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `tb_bumdes`
--
ALTER TABLE `tb_bumdes`
  ADD PRIMARY KEY (`id_bumdes`),
  ADD KEY `kode_kecamatan` (`kode_kecamatan`);

--
-- Indexes for table `tb_kabupaten`
--
ALTER TABLE `tb_kabupaten`
  ADD PRIMARY KEY (`kd_kab`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_kecamatan`
--
ALTER TABLE `tb_kecamatan`
  ADD PRIMARY KEY (`kd_kec`),
  ADD KEY `kd_kab` (`kd_kab`);

--
-- Indexes for table `tb_masalah`
--
ALTER TABLE `tb_masalah`
  ADD PRIMARY KEY (`id_masalah`);

--
-- Indexes for table `tb_unit`
--
ALTER TABLE `tb_unit`
  ADD PRIMARY KEY (`id_unit`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `unit_bumdes`
--
ALTER TABLE `unit_bumdes`
  ADD PRIMARY KEY (`id_bumdes`,`id_unit`),
  ADD UNIQUE KEY `id_unit` (`id_unit`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_bumdes`
--
ALTER TABLE `tb_bumdes`
  MODIFY `id_bumdes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_masalah`
--
ALTER TABLE `tb_masalah`
  MODIFY `id_masalah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_unit`
--
ALTER TABLE `tb_unit`
  MODIFY `id_unit` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `masalah_bumdes`
--
ALTER TABLE `masalah_bumdes`
  ADD CONSTRAINT `masalah_bumdes_ibfk_1` FOREIGN KEY (`id_bumdes`) REFERENCES `tb_bumdes` (`id_bumdes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `masalah_bumdes_ibfk_2` FOREIGN KEY (`id_masalah`) REFERENCES `tb_masalah` (`id_masalah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_bumdes`
--
ALTER TABLE `tb_bumdes`
  ADD CONSTRAINT `tb_bumdes_ibfk_1` FOREIGN KEY (`kode_kecamatan`) REFERENCES `tb_kecamatan` (`kd_kec`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_kecamatan`
--
ALTER TABLE `tb_kecamatan`
  ADD CONSTRAINT `tb_kecamatan_ibfk_1` FOREIGN KEY (`kd_kab`) REFERENCES `tb_kabupaten` (`kd_kab`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `unit_bumdes`
--
ALTER TABLE `unit_bumdes`
  ADD CONSTRAINT `unit_bumdes_ibfk_1` FOREIGN KEY (`id_bumdes`) REFERENCES `tb_bumdes` (`id_bumdes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `unit_bumdes_ibfk_2` FOREIGN KEY (`id_unit`) REFERENCES `tb_unit` (`id_unit`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
