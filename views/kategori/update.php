<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kategori */

$this->title = 'Update Kategori: ' . $model->nama_kategori;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Kategori', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_kategori, 'url' => ['view', 'id' => $model->id_kategori]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kategori-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
