<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unit-form">

    <?php $form = ActiveForm::begin(); ?>
        <?= $form->errorSummary($model) ?> <!-- ADDED HERE -->


    <?php $list = \yii\helpers\ArrayHelper::map(\app\models\Kategori::find()->orderBy('nama_kategori')->asArray()->all(), 'id_kategori', 'nama_kategori') ?>

          <?= $form->field($model, 'id_kategori')->widget(Select2::classname(), [
            'data' => $list,
            'pluginOptions'=>[
              'placeholder'=>'Pilih Kategori',
            ],
          ]); ?>

    <?= $form->field($model, 'kode_unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ket_unit')->textarea(['rows' => 6]) ?>

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
