<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$this->title = 'Update Unit: ' . $model->nama_unit;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Unit', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_unit, 'url' => ['view', 'id' => $model->id_unit]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="unit-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
