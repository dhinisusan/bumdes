<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Masalah */

$this->title = 'Update Masalah: ' . $model->nama_masalah;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Masalah', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_masalah, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="masalah-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
