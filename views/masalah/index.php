<?php


use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use app\widgets\grid\GridView;


$gridColumns=[['class' => 'yii\grid\SerialColumn'], 
            'kode_masalah',
            'nama_masalah:ntext',

         ['class' => 'app\widgets\grid\ActionColumn',   'template' => Mimin::filterActionColumn([
              'view','update','delete'],$this->context->route),    ],    ];


/* @var $this yii\web\View */
/* @var $searchModel app\models\MasalahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Masalah';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="masalah-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p> <?php if ((Mimin::checkRoute($this->context->id."/create"))){ ?>        <?=  Html::a('Masalah Baru', ['create'], ['class' => 'btn btn-success']) ?>
    <?php } ?>    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,      
    ]); ?>
</div>
