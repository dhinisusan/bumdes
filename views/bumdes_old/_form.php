<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Bumdes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bumdes-form">

  <?php $form = ActiveForm::begin(); ?>
  <?= $form->errorSummary($model) ?> 
  <!-- ADDED HERE -->
  <div class="card">

    <div class="card-body">
      <div class="row">
        <div class="col-md-6">

          <?php $catList = \yii\helpers\ArrayHelper::map(\app\models\Kabupaten::find()->orderBy('nama_kab')->asArray()->all(), 'kd_kab', 'nama_kab') ?>

          <?= $form->field($model, 'kode_kabupaten')->widget(Select2::classname(), [
            'data' => $catList,
            'pluginOptions'=>[
              'placeholder'=>'Pilih Kabupaten',
            ],
          ]); ?>

          <?=  $form->field($model, 'kode_kecamatan')->widget(DepDrop::classname(), [
            'type'=>DepDrop::TYPE_SELECT2,
            'options'=>['id'=>'bumdes-kode_kecamatan'],
            'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
            'pluginOptions'=>[
              'depends'=>['bumdes-kode_kabupaten'],
              'url'=>Url::to(['/bumdes/kecamatan']),
              'placeholder'=>'Pilih Kecamatan',
            ]
          ]); ?>


          <!-- $form->field($model, '')->textInput() ?> -->

          <?= $form->field($model, 'nama_bumdes')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'nama_desa')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

          <?= $form->field($model, 'no_perdes')->textInput(['maxlength' => true]) ?>


          <?= $form->field($model, 'adart')->widget(SwitchInput::classname(), [
            'pluginOptions' => [
              'onText' => 'Ya',
              'offText' => 'Tidak',
            ]
          ]) ?>


          <?= $form->field($model, 'rencana_men')->widget(SwitchInput::classname(), [
            'pluginOptions' => [
              'onText' => 'Ya',
              'offText' => 'Tidak',
            ]
          ]) ?>

          <?= $form->field($model, 'rencana_th')->widget(SwitchInput::classname(), [
            'pluginOptions' => [
              'onText' => 'Ya',
              'offText' => 'Tidak',
            ]
          ]) ?>

          <?= $form->field($model, 'sop')->widget(SwitchInput::classname(), [
            'pluginOptions' => [
              'onText' => 'Ya',
              'offText' => 'Tidak',
            ]
          ]) ?>
          <?= $form->field($model, 'catatan_keu')->widget(SwitchInput::classname(), [
            'pluginOptions' => [
              'onText' => 'Ya',
              'offText' => 'Tidak',
            ]
          ]) ?>

          <?= $form->field($model, 'status')->widget(SwitchInput::classname(), [
            'pluginOptions' => [
              'onText' => 'Aktif',
              'offText' => 'Tidak Aktif',
            ]
          ]) ?>


        </div>

        <div class="col-md-6">

          <?= $form->field($model, 'th_perdes')->textInput() ?>

          <?= $form->field($model, 'jml_omset_th')->textInput() ?>

          <?= $form->field($model, 'kontribusi_pen_desa')->textInput() ?>

          <?= $form->field($model, 'jml_modal')->textInput() ?>

          <?= $form->field($model, 'jml_modal_lain')->textInput() ?>

          <?= $form->field($model, 'jml_kas')->textInput() ?>

          <?= $form->field($model, 'jml_aset_lcr')->textInput() ?>

          <?= $form->field($model, 'jml_aset_ttp')->textInput() ?>

          <?= $form->field($model, 'jml_utang')->textInput() ?>

          <?= $form->field($model, 'jml_utang_bank')->textInput() ?>

          <?= $form->field($model, 'jml_unit_ush')->textInput() ?>


        </div>

        <div class="col-md-12 text-center">

         <div class="form-group">
          <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

      </div>

    </div>

  </div>

</div>






<?php ActiveForm::end(); ?>

</div>
