<?php


use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use app\widgets\grid\GridView;


$gridColumns=[['class' => 'yii\grid\SerialColumn'], 
            // 'kode_bumdes',
            'nama_bumdes',
            'nama_desa',
            'alamat:ntext',
            [
                'attribute' => 'status',
                'filter' => [0 => 'Tidak Aktif', 1 => 'Aktif'],
                'format' => 'raw',
                'options' => [
                    'width' => '80px',
                ],
                'value' => function ($data) {
                    if ($data->status == 1)
                        return "<span class='label label-primary'>" . 'Aktif' . "</span>";
                    else
                        return "<span class='label label-danger'>" . 'Tidak Aktif' . "</span>";
                }
            ],
            // 'no_perdes',
            // 'th_perdes',
            // 'adart',
            // 'rencana_men',
            // 'rencana_th',
            // 'sop',
            // 'catatan_keu',
            // 'jml_omset_th',
            // 'kontribusi_pen_desa',
            // 'jml_modal',
            // 'jml_modal_lain',
            // 'jml_kas',
            // 'jml_aset_lcr',
            // 'jml_aset_ttp',
            // 'jml_utang',
            // 'jml_utang_bank',
            // 'jml_unit_ush',
            // 'kode_kabupaten',
            // 'kode_kecamatan',

         ['class' => 'app\widgets\grid\ActionColumn',   'template' => Mimin::filterActionColumn([
              'view','update','delete'],$this->context->route),    ],    ];


/* @var $this yii\web\View */
/* @var $searchModel app\models\BumdesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Bumdes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p> <?php if ((Mimin::checkRoute($this->context->id."/create"))){ ?>        <?=  Html::a('Bumdes Baru', ['create'], ['class' => 'btn btn-success']) ?>
    <?php } ?>    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,      
    ]); ?>
</div>
