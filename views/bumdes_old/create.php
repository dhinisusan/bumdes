<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bumdes */

$this->title = 'Bumdes Baru';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
