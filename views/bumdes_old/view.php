<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use hscstudio\mimin\components\Mimin;

/* @var $this yii\web\View */
/* @var $model app\models\Bumdes */

$this->title = $model->nama_bumdes;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-view">

    <p>
     <?php if ((Mimin::checkRoute($this->context->id."/update"))){ ?>        <?= Html::a('Ubah', ['update', 'id' => $model->id_bumdes], ['class' => 'btn btn-primary']) ?>
 <?php } if ((Mimin::checkRoute($this->context->id."/delete"))){ ?>        <?= Html::a('Hapus', ['delete', 'id' => $model->id_bumdes], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Apakah Anda yakin ingin menghapus item ini??',
        'method' => 'post',
    ],
]) ?>
<?php } ?>    </p>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'kode_kabupaten',
        'kode_kecamatan',
        'nama_bumdes',
        'nama_desa',
        'alamat:ntext',
        'no_perdes',
        'th_perdes',
        [
            'attribute' => 'adart',
            'value' =>  $model->adart == 1 ? 'Ya':'Tidak', 
        ],
        [
            'attribute' => 'rencana_men',
            'value' =>  $model->rencana_men == 1 ? 'Ya':'Tidak', 
        ],
        [
            'attribute' => 'rencana_th',
            'value' =>  $model->rencana_th == 1 ? 'Ya':'Tidak', 
        ],
        [
            'attribute' => 'sop',
            'value' =>  $model->sop == 1 ? 'Ya':'Tidak', 
        ],
        [
            'attribute' => 'catatan_keu',
            'value' =>  $model->catatan_keu == 1 ? 'Ya':'Tidak', 
        ],
        'jml_omset_th',
        'kontribusi_pen_desa',
        'jml_modal',
        'jml_modal_lain',
        'jml_kas',
        'jml_aset_lcr',
        'jml_aset_ttp',
        'jml_utang',
        'jml_utang_bank',
        'jml_unit_ush',
    ],
]) ?>

</div>
