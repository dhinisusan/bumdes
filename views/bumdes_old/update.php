<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bumdes */

$this->title = 'Update Bumdes: ' . $model->nama_bumdes;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_bumdes, 'url' => ['view', 'id' => $model->id_bumdes]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
