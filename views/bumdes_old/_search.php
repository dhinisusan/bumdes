<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BumdesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bumdes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_bumdes') ?>

    <?= $form->field($model, 'kode_bumdes') ?>

    <?= $form->field($model, 'nama_bumdes') ?>

    <?= $form->field($model, 'nama_desa') ?>

    <?= $form->field($model, 'alamat') ?>

    <?php // echo $form->field($model, 'no_perdes') ?>

    <?php // echo $form->field($model, 'th_perdes') ?>

    <?php // echo $form->field($model, 'adart') ?>

    <?php // echo $form->field($model, 'rencana_men') ?>

    <?php // echo $form->field($model, 'rencana_th') ?>

    <?php // echo $form->field($model, 'sop') ?>

    <?php // echo $form->field($model, 'catatan_keu') ?>

    <?php // echo $form->field($model, 'jml_omset_th') ?>

    <?php // echo $form->field($model, 'kontribusi_pen_desa') ?>

    <?php // echo $form->field($model, 'jml_modal') ?>

    <?php // echo $form->field($model, 'jml_modal_lain') ?>

    <?php // echo $form->field($model, 'jml_kas') ?>

    <?php // echo $form->field($model, 'jml_aset_lcr') ?>

    <?php // echo $form->field($model, 'jml_aset_ttp') ?>

    <?php // echo $form->field($model, 'jml_utang') ?>

    <?php // echo $form->field($model, 'jml_utang_bank') ?>

    <?php // echo $form->field($model, 'jml_unit_ush') ?>

    <?php // echo $form->field($model, 'kode_kabupaten') ?>

    <?php // echo $form->field($model, 'kode_kecamatan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
