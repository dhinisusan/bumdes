<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bumdes */

$this->title = 'Update Bumdes: ' . ' ' . $model->id_bumdes;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_bumdes, 'url' => ['view', 'id' => $model->id_bumdes]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bumdes-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
