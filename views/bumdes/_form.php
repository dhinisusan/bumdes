<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Bumdes */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MasalahBumdes', 
        'relID' => 'masalah-bumdes', 
        'value' => \yii\helpers\Json::encode($model->masalahBumdes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'UnitBumdes', 
        'relID' => 'unit-bumdes', 
        'value' => \yii\helpers\Json::encode($model->unitBumdes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="bumdes-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card">
        <div class="card-body">

            <?= $form->errorSummary($model); ?>
            <div class="row">
                <div class="col-md-6">


                  <?php $catList = \yii\helpers\ArrayHelper::map(\app\models\Kabupaten::find()->orderBy('nama_kab')->asArray()->all(), 'kd_kab', 'nama_kab') ?>

                  <?= $form->field($model, 'kode_kabupaten')->widget(Select2::classname(), [
                    'data' => $catList,
                    'pluginOptions'=>[
                      'placeholder'=>'Pilih Kabupaten',
                  ],
              ]); ?>

              <?php echo Html::hiddenInput('selected_id', $model->isNewRecord ? '' : $model->kode_kecamatan, ['id'=>'selected_id']); ?>

              <?php 

              $x = \app\models\Kecamatan::find()->where(['kd_kab'=>$model->kode_kabupaten])->all();
                $y=[];
                if ($x!=null) {
                  foreach ($x as $key => $value) {
                    # code...
                  }
                  $y =[
                    $value['kd_kec']=>$value['nama_kec'],
                  ];
                }

              ?>


                  <?=  $form->field($model, 'kode_kecamatan')->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'data'=>$y,
                    'options'=>['id'=>'bumdes-kode_kecamatan',],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                      'depends'=>['bumdes-kode_kabupaten'],
                      'initialize' => true,
                      'url'=>Url::to(['/bumdes/kecamatan']),
                      // 'placeholder'=>'Pilih Kecamatan',
                      'params'=> ['selected_id'], 

                  ]
              ]); ?>


              <!-- $form->field($model, '')->textInput() ?> -->

              <?= $form->field($model, 'nama_bumdes')->textInput(['maxlength' => true]) ?>

              <?= $form->field($model, 'nama_desa')->textInput(['maxlength' => true]) ?>

              <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

              <?= $form->field($model, 'no_perdes')->textInput(['maxlength' => true]) ?>


              <?= $form->field($model, 'adart')->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                  'onText' => 'Ya',
                  'offText' => 'Tidak',
              ]
          ]) ?>


              <?= $form->field($model, 'rencana_men')->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                  'onText' => 'Ya',
                  'offText' => 'Tidak',
              ]
          ]) ?>

              <?= $form->field($model, 'rencana_th')->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                  'onText' => 'Ya',
                  'offText' => 'Tidak',
              ]
          ]) ?>

              <?= $form->field($model, 'sop')->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                  'onText' => 'Ya',
                  'offText' => 'Tidak',
              ]
          ]) ?>
              <?= $form->field($model, 'catatan_keu')->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                  'onText' => 'Ya',
                  'offText' => 'Tidak',
              ]
          ]) ?>

              <?= $form->field($model, 'status')->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                  'onText' => 'Aktif',
                  'offText' => 'Tidak Aktif',
              ]
          ]) ?>
      </div>

      <div class="col-md-6">
        <?= $form->field($model, 'th_perdes')->textInput() ?>

        <?= $form->field($model, 'jml_omset_th')->textInput() ?>

        <?= $form->field($model, 'kontribusi_pen_desa')->textInput() ?>

        <?= $form->field($model, 'jml_modal')->textInput() ?>

        <?= $form->field($model, 'jml_modal_lain')->textInput() ?>

        <?= $form->field($model, 'jml_kas')->textInput() ?>

        <?= $form->field($model, 'jml_aset_lcr')->textInput() ?>

        <?= $form->field($model, 'jml_aset_ttp')->textInput() ?>

        <?= $form->field($model, 'jml_utang')->textInput() ?>

        <?= $form->field($model, 'jml_utang_bank')->textInput() ?>

        <?= $form->field($model, 'jml_unit_ush')->textInput() ?>

    </div>

    <div class="col-md-12">
      <?php
      $forms = [
        [
            'label' => '<i class="fas fa-book"></i> ' . Html::encode('MasalahBumdes'),
            'content' => $this->render('_formMasalahBumdes', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->masalahBumdes),
            ]),
        ],
        [
            'label' => '<i class="fas fa-book"></i> ' . Html::encode('UnitBumdes'),
            'content' => $this->render('_formUnitBumdes', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->unitBumdes),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
</div>

<div class="col-md-12 text-center">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-info']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-secondary']) ?>
</div>

</div>

</div>

</div>
<?php ActiveForm::end(); ?>
</div>
