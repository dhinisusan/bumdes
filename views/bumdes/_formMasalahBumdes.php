<div class="form-group" id="add-masalah-bumdes">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'MasalahBumdes',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'id_masalah' => [
            'label' => 'Tb masalah',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Masalah::find()->orderBy('id_masalah')->asArray()->all(), 'id_masalah', 'nama_masalah'),
                'options' => ['placeholder' => 'Choose Tb masalah'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="fas fa-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMasalahBumdes(' . $key . '); return false;', 'id' => 'masalah-bumdes-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="fas fa-plus"></i>' . 'Add Masalah Bumdes', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowMasalahBumdes()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

