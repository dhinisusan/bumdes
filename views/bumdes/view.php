<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Bumdes */

$this->title = $model->nama_bumdes;
$this->params['breadcrumbs'][] = ['label' => 'Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-view">

    <div class="row">
        <div class="col-sm-9">

        </div>
        <div class="col-sm-3" style="margin-top: 10px">

            <?= Html::a('Update', ['update', 'id' => $model->id_bumdes], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id_bumdes], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <?php 
                    $gridColumn = [
                        // 'id_bumdes',
                        // 'kode_bumdes',
                        'nama_bumdes',
                        'nama_desa',
                        'alamat:ntext',
                        'no_perdes',
                        'th_perdes',
                        'adart',
                        'rencana_men',
                        'rencana_th',
                        'sop',
                        'catatan_keu',
                        'jml_omset_th',
                        'kontribusi_pen_desa',
                        'jml_modal',
                        'jml_modal_lain',
                        'jml_kas',
                        'jml_aset_lcr',
                        'jml_aset_ttp',
                        'jml_utang',
                        'jml_utang_bank',
                        'jml_unit_ush',
                        'status',
                        [
                            'attribute' => 'kodeKecamatan.kabupaten.nama_kab',
                            'label' => ' Kabupaten',
                        ],
                        [
                            'attribute' => 'kodeKecamatan.nama_kec',
                            'label' => ' Kecamatan',
                        ],
                    ];
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                    ?>

                </div>

            </div>
        </div>

    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?php
            if($providerMasalahBumdes->totalCount){
                $gridColumnMasalahBumdes = [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'masalah.nama_masalah',
                        'label' => 'Masalah'
                    ],
                ];
                echo Gridview::widget([
                    'dataProvider' => $providerMasalahBumdes,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-masalah-bumdes']],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Masalah Bumdes'),
                    ],
                    'export' => false,
                    'columns' => $gridColumnMasalahBumdes
                ]);
            }
            ?>
        </div>
        <div class="col-md-6">
           <?php
           if($providerUnitBumdes->totalCount){
            $gridColumnUnitBumdes = [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'unit.nama_unit',
                    'label' => ' Unit'
                ],
                [
                    'attribute' => 'unit.nama_unit',
                    'label' => ' Unit'
                ],
            ];
            echo Gridview::widget([
                'dataProvider' => $providerUnitBumdes,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-unit-bumdes']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Unit Bumdes'),
                ],
                'export' => false,
                'columns' => $gridColumnUnitBumdes
            ]);
        }
        ?>
    </div>




</div>
</div>
