<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Bumdes */

?>
<div class="bumdes-view">

    <div class="row">
        <div class="col-sm-9">
            
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        // 'id_bumdes',
        // 'kode_bumdes',
        // 'nama_bumdes',
        // 'nama_desa',
        // 'alamat:ntext',
        // 'no_perdes',
        // 'th_perdes',
        'adart',
        'rencana_men',
        'rencana_th',
        'sop',
        'catatan_keu',
        'jml_omset_th',
        'kontribusi_pen_desa',
        'jml_modal',
        'jml_modal_lain',
        'jml_kas',
        'jml_aset_lcr',
        'jml_aset_ttp',
        'jml_utang',
        'jml_utang_bank',
        'jml_unit_ush',
        'status',
        'kode_kabupaten',
        [
            'attribute' => 'kodeKecamatan.nama_kec',
            'label' => 'Kecamatan',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>