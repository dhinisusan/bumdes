<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bumdes */

$this->title = 'Create Bumdes';
$this->params['breadcrumbs'][] = ['label' => 'Bumdes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bumdes-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
