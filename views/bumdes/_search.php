<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BumdesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-bumdes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php // $form->field($model, 'id_bumdes')->textInput(['placeholder' => 'Id Bumdes']) ?>

    <?php //$form->field($model, 'kode_bumdes')->textInput(['maxlength' => true, 'placeholder' => 'Kode Bumdes']) ?>

    <?= $form->field($model, 'nama_bumdes')->textInput(['maxlength' => true, 'placeholder' => 'Nama Bumdes']) ?>

    <?= $form->field($model, 'nama_desa')->textInput(['maxlength' => true, 'placeholder' => 'Nama Desa']) ?>

    <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

    <?php /* echo $form->field($model, 'no_perdes')->textInput(['maxlength' => true, 'placeholder' => 'No Perdes']) */ ?>

    <?php /* echo $form->field($model, 'th_perdes')->textInput(['placeholder' => 'Th Perdes']) */ ?>

    <?php /* echo $form->field($model, 'adart')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'rencana_men')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'rencana_th')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'sop')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'catatan_keu')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'jml_omset_th')->textInput(['placeholder' => 'Jml Omset Th']) */ ?>

    <?php /* echo $form->field($model, 'kontribusi_pen_desa')->textInput(['placeholder' => 'Kontribusi Pen Desa']) */ ?>

    <?php /* echo $form->field($model, 'jml_modal')->textInput(['placeholder' => 'Jml Modal']) */ ?>

    <?php /* echo $form->field($model, 'jml_modal_lain')->textInput(['placeholder' => 'Jml Modal Lain']) */ ?>

    <?php /* echo $form->field($model, 'jml_kas')->textInput(['placeholder' => 'Jml Kas']) */ ?>

    <?php /* echo $form->field($model, 'jml_aset_lcr')->textInput(['placeholder' => 'Jml Aset Lcr']) */ ?>

    <?php /* echo $form->field($model, 'jml_aset_ttp')->textInput(['placeholder' => 'Jml Aset Ttp']) */ ?>

    <?php /* echo $form->field($model, 'jml_utang')->textInput(['placeholder' => 'Jml Utang']) */ ?>

    <?php /* echo $form->field($model, 'jml_utang_bank')->textInput(['placeholder' => 'Jml Utang Bank']) */ ?>

    <?php /* echo $form->field($model, 'jml_unit_ush')->textInput(['placeholder' => 'Jml Unit Ush']) */ ?>

    <?php /* echo $form->field($model, 'status')->textInput(['placeholder' => 'Status']) */ ?>

    <?php /* echo $form->field($model, 'kode_kabupaten')->textInput(['maxlength' => true, 'placeholder' => 'Kode Kabupaten']) */ ?>

    <?php /* echo $form->field($model, 'kode_kecamatan')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TbKecamatan::find()->orderBy('kd_kec')->asArray()->all(), 'kd_kec', 'kd_kec'),
        'options' => ['placeholder' => 'Choose Tb kecamatan'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
