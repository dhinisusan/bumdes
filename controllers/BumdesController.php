<?php

namespace app\controllers;

use Yii;
use app\models\Bumdes;
use app\models\BumdesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BumdesController implements the CRUD actions for Bumdes model.
 */
class BumdesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bumdes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BumdesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bumdes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMasalahBumdes = new \yii\data\ArrayDataProvider([
            'allModels' => $model->masalahBumdes,
        ]);
        $providerUnitBumdes = new \yii\data\ArrayDataProvider([
            'allModels' => $model->unitBumdes,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMasalahBumdes' => $providerMasalahBumdes,
            'providerUnitBumdes' => $providerUnitBumdes,
        ]);
    }

    /**
     * Creates a new Bumdes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bumdes();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id_bumdes]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Bumdes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id_bumdes]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Bumdes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the Bumdes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bumdes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bumdes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MasalahBumdes
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMasalahBumdes()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MasalahBumdes');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMasalahBumdes', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for UnitBumdes
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddUnitBumdes()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('UnitBumdes');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formUnitBumdes', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionKecamatan() 
    {
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $cat_id = end($_POST['depdrop_parents']);
            $list = \app\models\Kecamatan::find()
            ->where(['kd_kab'=>$cat_id])
            ->select(['kd_kec','nama_kec'])->asArray()->all();
            // $selected  = null;
            $selected_id = $_POST['depdrop_all_params']['selected_id']; 
            if ($cat_id != null && count($list) > 0) {
                // $selected = '';
                foreach ($list as $i => $row) {
                    $out[] = ['id' => $row['kd_kec'], 'name' => $row['nama_kec']];
                    // if ($i == 0) {
                    //     $selected = $row['kd_kec'];
                    // }
                }
            // Shows how you can preselect a value
                return ['output' => $out, 'selected' => $selected_id];
            }
        }
        return ['output' => '', 'selected' => ''];
    }
}
