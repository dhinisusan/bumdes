<?php

namespace app\models;

use Yii;
use \app\models\base\UnitBumdes as BaseUnitBumdes;

/**
 * This is the model class for table "unit_bumdes".
 */
class UnitBumdes extends BaseUnitBumdes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id_bumdes', 'id_unit'], 'required'],
            [['id_bumdes', 'id_unit'], 'integer'],
            // [['id_unit'], 'unique'],
            // [['lock'], 'default', 'value' => '0'],
            // [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
