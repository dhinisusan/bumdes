<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_masalah".
 *
 * @property int $id
 * @property string $kode_masalah
 * @property string $nama_masalah
 */
class Masalah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_masalah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_masalah', 'nama_masalah'], 'required'],
            [['kode_masalah'], 'unique'],
            [['nama_masalah'], 'string'],
            [['kode_masalah'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_masalah' => 'Kode Masalah',
            'nama_masalah' => 'Nama Masalah',
        ];
    }
}
