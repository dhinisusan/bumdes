<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "tb_bumdes".
 *
 * @property integer $id_bumdes
 * @property string $kode_bumdes
 * @property string $nama_bumdes
 * @property string $nama_desa
 * @property string $alamat
 * @property string $no_perdes
 * @property integer $th_perdes
 * @property integer $adart
 * @property integer $rencana_men
 * @property integer $rencana_th
 * @property integer $sop
 * @property integer $catatan_keu
 * @property double $jml_omset_th
 * @property double $kontribusi_pen_desa
 * @property double $jml_modal
 * @property double $jml_modal_lain
 * @property double $jml_kas
 * @property double $jml_aset_lcr
 * @property double $jml_aset_ttp
 * @property double $jml_utang
 * @property double $jml_utang_bank
 * @property double $jml_unit_ush
 * @property integer $status
 * @property string $kode_kabupaten
 * @property string $kode_kecamatan
 *
 * @property \app\models\MasalahBumdes[] $masalahBumdes
<<<<<<< HEAD
 * @property \app\models\TbMasalah[] $masalahs
 * @property \app\models\TbKecamatan $kodeKecamatan
 * @property \app\models\UnitBumdes[] $unitBumdes
 * @property \app\models\TbUnit[] $units
=======
 * @property \app\models\Masalah[] $masalahs
 * @property \app\models\Kecamatan $kodeKecamatan
 * @property \app\models\UnitBumdes[] $unitBumdes
 * @property \app\models\Unit[] $units
>>>>>>> 3369512d6cfed8efc12f917c9637b5dd34c207d3
 */
class Bumdes extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'masalahBumdes',
            'masalahs',
            'kodeKecamatan',
            'unitBumdes',
            'units'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_bumdes', 'nama_desa', 'alamat', 'no_perdes', 'th_perdes'], 'required'],
            [['alamat'], 'string'],
            [['th_perdes'], 'integer'],
            [['jml_omset_th', 'kontribusi_pen_desa', 'jml_modal', 'jml_modal_lain', 'jml_kas', 'jml_aset_lcr', 'jml_aset_ttp', 'jml_utang', 'jml_utang_bank', 'jml_unit_ush'], 'number'],
            [['kode_bumdes'], 'string', 'max' => 20],
            [['nama_bumdes', 'nama_desa', 'no_perdes'], 'string', 'max' => 200],
            [['adart', 'rencana_men', 'rencana_th', 'sop', 'catatan_keu'], 'string', 'max' => 1],
            [['status'], 'string', 'max' => 4],
            [['kode_kabupaten', 'kode_kecamatan'], 'string', 'max' => 100],

            // [['lock'], 'default', 'value' => '0'],
            // [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_bumdes';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    // public function optimisticLock() {
    //     return 'lock';
    // }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_bumdes' => 'Id Bumdes',
            'kode_bumdes' => 'Kode Bumdes',
            'nama_bumdes' => 'Nama Bumdes',
            'nama_desa' => 'Nama Desa',
            'alamat' => 'Alamat',
            'no_perdes' => 'No Perdes',
            'th_perdes' => 'Th Perdes',
            'adart' => 'Adart',
            'rencana_men' => 'Rencana Men',
            'rencana_th' => 'Rencana Th',
            'sop' => 'Sop',
            'catatan_keu' => 'Catatan Keu',
            'jml_omset_th' => 'Jml Omset Th',
            'kontribusi_pen_desa' => 'Kontribusi Pen Desa',
            'jml_modal' => 'Jml Modal',
            'jml_modal_lain' => 'Jml Modal Lain',
            'jml_kas' => 'Jml Kas',
            'jml_aset_lcr' => 'Jml Aset Lcr',
            'jml_aset_ttp' => 'Jml Aset Ttp',
            'jml_utang' => 'Jml Utang',
            'jml_utang_bank' => 'Jml Utang Bank',
            'jml_unit_ush' => 'Jml Unit Ush',
            'status' => 'Status',
            'kode_kabupaten' => 'Kode Kabupaten',
            'kode_kecamatan' => 'Kode Kecamatan',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasalahBumdes()
    {
        return $this->hasMany(\app\models\MasalahBumdes::className(), ['id_bumdes' => 'id_bumdes']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasalahs()
    {
        return $this->hasMany(\app\models\Masalah::className(), ['id_masalah' => 'id_masalah'])->viaTable('masalah_bumdes', ['id_bumdes' => 'id_bumdes']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKodeKecamatan()
    {
        return $this->hasOne(\app\models\Kecamatan::className(), ['kd_kec' => 'kode_kecamatan']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitBumdes()
    {
        return $this->hasMany(\app\models\UnitBumdes::className(), ['id_bumdes' => 'id_bumdes']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(\app\models\Unit::className(), ['id_unit' => 'id_unit'])->viaTable('unit_bumdes', ['id_bumdes' => 'id_bumdes']);
    }
    

    /**
     * @inheritdoc
     * @return \app\models\BumdesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\BumdesQuery(get_called_class());
    }
}
