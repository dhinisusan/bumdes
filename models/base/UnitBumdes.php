<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "unit_bumdes".
 *
 * @property integer $id_bumdes
 * @property integer $id_unit
 *
 * @property \app\models\TbBumdes $bumdes
 * @property \app\models\TbUnit $unit
 */
class UnitBumdes extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    // private $_rt_softdelete;
    // private $_rt_softrestore;

    // public function __construct(){
    //     parent::__construct();
    //     $this->_rt_softdelete = [
    //         'deleted_at' => date('Y-m-d H:i:s'),
    //     ];
    //     $this->_rt_softrestore = [
    //         'deleted_at' => date('Y-m-d H:i:s'),
    //     ];
    // }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'bumdes',
            'unit'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_bumdes', 'id_unit'], 'required'],
            [['id_bumdes', 'id_unit'], 'integer'],
            // [['id_unit'], 'unique'],
            // [['lock'], 'default', 'value' => '0'],
            // [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit_bumdes';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    // public function optimisticLock() {
    //     return 'lock';
    // }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_bumdes' => 'Id Bumdes',
            'id_unit' => 'Id Unit',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBumdes()
    {
        return $this->hasOne(\app\models\Bumdes::className(), ['id_bumdes' => 'id_bumdes']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(\app\models\Unit::className(), ['id_unit' => 'id_unit']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    // public function behaviors()
    // {
    //     return [
    //         'timestamp' => [
    //             'class' => TimestampBehavior::className(),
    //             'createdAtAttribute' => 'created_at',
    //             'updatedAtAttribute' => 'updated_at',
    //             'value' => new \yii\db\Expression('NOW()'),
    //         ],
    //         'blameable' => [
    //             'class' => BlameableBehavior::className(),
    //             'createdByAttribute' => 'created_by',
    //             'updatedByAttribute' => 'updated_by',
    //         ],
    //         'uuid' => [
    //             'class' => UUIDBehavior::className(),
    //             'column' => 'id',
    //         ],
    //     ];
    // }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \app\models\UnitBumdesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\UnitBumdesQuery(get_called_class());
    }
}
