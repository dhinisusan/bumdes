<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "masalah_bumdes".
 *
 * @property integer $id_bumdes
 * @property integer $id_masalah
 *
 * @property \app\models\TbBumdes $bumdes
 * @property \app\models\TbMasalah $masalah
 */
class MasalahBumdes extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'bumdes',
            'masalah'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_bumdes', 'id_masalah'], 'required'],
            [['id_bumdes', 'id_masalah'], 'integer'],
            // [['lock'], 'default', 'value' => '0'],
            // [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'masalah_bumdes';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    // public function optimisticLock() {
    //     return 'lock';
    // }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_bumdes' => 'Id Bumdes',
            'id_masalah' => 'Id Masalah',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBumdes()
    {
        return $this->hasOne(\app\models\Bumdes::className(), ['id_bumdes' => 'id_bumdes']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasalah()
    {
        return $this->hasOne(\app\models\Masalah::className(), ['id_masalah' => 'id_masalah']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    // public function behaviors()
    // {
    //     return [
    //         'timestamp' => [
    //             'class' => TimestampBehavior::className(),
    //             'createdAtAttribute' => 'created_at',
    //             'updatedAtAttribute' => 'updated_at',
    //             'value' => new \yii\db\Expression('NOW()'),
    //         ],
    //         'blameable' => [
    //             'class' => BlameableBehavior::className(),
    //             'createdByAttribute' => 'created_by',
    //             'updatedByAttribute' => 'updated_by',
    //         ],
    //         'uuid' => [
    //             'class' => UUIDBehavior::className(),
    //             'column' => 'id',
    //         ],
    //     ];
    // }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \app\models\MasalahBumdesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MasalahBumdesQuery(get_called_class());
     
    }
}
