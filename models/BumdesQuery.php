<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Bumdes]].
 *
 * @see Bumdes
 */
class BumdesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Bumdes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bumdes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
