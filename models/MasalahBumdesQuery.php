<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MasalahBumdes]].
 *
 * @see MasalahBumdes
 */
class MasalahBumdesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MasalahBumdes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MasalahBumdes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
