<?php

namespace app\models;

use Yii;
use \app\models\base\Bumdes as BaseBumdes;

/**
 * This is the model class for table "tb_bumdes".
 */
class Bumdes extends BaseBumdes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nama_bumdes', 'nama_desa', 'alamat', 'no_perdes', 'th_perdes'], 'required'],
            [['alamat'], 'string'],
            [['th_perdes'], 'integer'],
            [['jml_omset_th', 'kontribusi_pen_desa', 'jml_modal', 'jml_modal_lain', 'jml_kas', 'jml_aset_lcr', 'jml_aset_ttp', 'jml_utang', 'jml_utang_bank', 'jml_unit_ush'], 'number'],
            [['kode_bumdes'], 'string', 'max' => 20],
            [['nama_bumdes', 'nama_desa', 'no_perdes'], 'string', 'max' => 200],
            [['adart', 'rencana_men', 'rencana_th', 'sop', 'catatan_keu'], 'string', 'max' => 1],
            [['status'], 'string', 'max' => 4],
            [['kode_kabupaten', 'kode_kecamatan'], 'string', 'max' => 100],
            // [['lock'], 'default', 'value' => '0'],
            // [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
