<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UnitBumdes]].
 *
 * @see UnitBumdes
 */
class UnitBumdesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UnitBumdes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UnitBumdes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
