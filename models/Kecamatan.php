<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_kecamatan".
 *
 * @property string $kd_kec
 * @property string $nama_kec
 * @property string|null $kd_kab
 */
class Kecamatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_kecamatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_kec', 'nama_kec'], 'required'],
            [['kd_kec', 'kd_kab'], 'string', 'max' => 100],
            [['nama_kec'], 'string', 'max' => 252],
            [['kd_kec'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_kec' => 'Kd Kec',
            'nama_kec' => 'Nama Kec',
            'kd_kab' => 'Kd Kab',
        ];
    }


    /**
     * Gets query for [[Regency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKabupaten()
    {
        return $this->hasOne(Kabupaten::className(), ['kd_kab' => 'kd_kab']);
    }
}
