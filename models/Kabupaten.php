<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_kabupaten".
 *
 * @property string $kd_kab
 * @property string $nama_kab
 */
class Kabupaten extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_kabupaten';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_kab', 'nama_kab'], 'required'],
            [['kd_kab'], 'string', 'max' => 100],
            [['nama_kab'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_kab' => 'Kd Kab',
            'nama_kab' => 'Nama Kab',
        ];
    }

    /**
     * Gets query for [[Kecamatan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatan()
    {
        return $this->hasMany(District::className(), ['kd_kab' => 'kd_kab']);
    }

}
