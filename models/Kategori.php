<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_kategori".
 *
 * @property int $id_kategori
 * @property string $nama_kategori
 * @property string $ket
 */
class Kategori extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_kategori';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_kategori', 'ket'], 'required'],
            [['ket'], 'string'],
            [['nama_kategori'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'nama_kategori' => 'Nama Kategori',
            'ket' => 'Ket',
        ];
    }


    /**
     * Gets query for [[Unit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasMany(Unit::className(), ['id_kategori' => 'id_kategori']);
    }

    
}
