<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_unit".
 *
 * @property int $id_unit
 * @property string $kode_unit
 * @property string $nama_unit
 * @property string $ket_unit
 * @property int|null $id_kategori
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_unit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_unit', 'nama_unit', 'ket_unit'], 'required'],
            [['ket_unit'], 'string'],
            [['ket_unit'], 'unique'],
            [['id_kategori'], 'integer'],
            [['kode_unit'], 'string', 'max' => 20],
            [['nama_unit'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_unit' => 'Id Unit',
            'kode_unit' => 'Kode Unit',
            'nama_unit' => 'Nama Unit',
            'ket_unit' => 'Ket Unit',
            'id_kategori' => 'Id Kategori',
        ];
    }

    /**
     * Gets query for [[Kategori]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKategori()
    {
        return $this->hasOne(Kategori::className(), ['id_kategori' => 'id_kategori']);
    }
}
