<?php

namespace app\models;

use Yii;
use \app\models\base\MasalahBumdes as BaseMasalahBumdes;

/**
 * This is the model class for table "masalah_bumdes".
 */
class MasalahBumdes extends BaseMasalahBumdes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id_bumdes', 'id_masalah'], 'required'],
            [['id_bumdes', 'id_masalah'], 'integer'],
            // [['lock'], 'default', 'value' => '0'],
            // [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
