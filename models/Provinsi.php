<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_prov".
 *
 * @property string $kd_prov
 * @property string $nama_prov
 */
class Provinsi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_prov';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_prov', 'nama_prov'], 'required'],
            [['kd_prov', 'nama_prov'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_prov' => 'Kd Prov',
            'nama_prov' => 'Nama Prov',
        ];
    }
}
