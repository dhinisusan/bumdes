<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bumdes;

/**
 * app\models\BumdesSearch represents the model behind the search form about `app\models\Bumdes`.
 */
 class BumdesSearch extends Bumdes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_bumdes', 'th_perdes'], 'integer'],
            [['kode_bumdes', 'nama_bumdes', 'nama_desa', 'alamat', 'no_perdes', 'adart', 'rencana_men', 'rencana_th', 'sop', 'catatan_keu', 'status', 'kode_kabupaten', 'kode_kecamatan'], 'safe'],
            [['jml_omset_th', 'kontribusi_pen_desa', 'jml_modal', 'jml_modal_lain', 'jml_kas', 'jml_aset_lcr', 'jml_aset_ttp', 'jml_utang', 'jml_utang_bank', 'jml_unit_ush'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bumdes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_bumdes' => $this->id_bumdes,
            'th_perdes' => $this->th_perdes,
            'jml_omset_th' => $this->jml_omset_th,
            'kontribusi_pen_desa' => $this->kontribusi_pen_desa,
            'jml_modal' => $this->jml_modal,
            'jml_modal_lain' => $this->jml_modal_lain,
            'jml_kas' => $this->jml_kas,
            'jml_aset_lcr' => $this->jml_aset_lcr,
            'jml_aset_ttp' => $this->jml_aset_ttp,
            'jml_utang' => $this->jml_utang,
            'jml_utang_bank' => $this->jml_utang_bank,
            'jml_unit_ush' => $this->jml_unit_ush,
        ]);

        $query->andFilterWhere(['like', 'kode_bumdes', $this->kode_bumdes])
            ->andFilterWhere(['like', 'nama_bumdes', $this->nama_bumdes])
            ->andFilterWhere(['like', 'nama_desa', $this->nama_desa])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'no_perdes', $this->no_perdes])
            ->andFilterWhere(['like', 'adart', $this->adart])
            ->andFilterWhere(['like', 'rencana_men', $this->rencana_men])
            ->andFilterWhere(['like', 'rencana_th', $this->rencana_th])
            ->andFilterWhere(['like', 'sop', $this->sop])
            ->andFilterWhere(['like', 'catatan_keu', $this->catatan_keu])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'kode_kabupaten', $this->kode_kabupaten])
            ->andFilterWhere(['like', 'kode_kecamatan', $this->kode_kecamatan]);

        return $dataProvider;
    }
}
